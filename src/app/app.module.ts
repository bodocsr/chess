import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { FieldComponent } from './components/field/field.component';
import { BoardComponent } from './components/board/board.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [AppComponent, FieldComponent, BoardComponent],
  imports: [BrowserModule, AppRoutingModule, StoreModule.forRoot({}, {}), CommonModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
