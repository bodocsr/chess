export enum PieceEnum {
  WhiteBishop = 'white_bishop',
  WhitePawn = 'white_pawn',
  WhiteHorse = 'white_horse',
  WhiteRook = 'white_rook',
  WhiteKing = 'white_king',
  WhiteQueen = 'white_queen',
  BlackBishop = 'black_bishop',
  BlackPawn = 'black_pawn',
  BlackHorse = 'black_horse',
  BlackRook = 'black_rook',
  BlackKing = 'black_king',
  BlackQueen = 'black_queen',
  Empty = 'empty'
}
