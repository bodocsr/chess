import { Injectable } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';
import { ColumnRowEnum } from 'src/app/enums/column-row-index.enum';
import { PieceEnum } from 'src/app/enums/pieces.enum';




export interface State {
  pieces: PieceEnum[][],
  selectedPiece: ColumnRowEnum,
  selectedCanMove: ColumnRowEnum[]
};

const defaultState: State = {
  pieces: [
    [PieceEnum.BlackRook,PieceEnum.BlackHorse,PieceEnum.BlackBishop,PieceEnum.BlackQueen,PieceEnum.BlackKing,PieceEnum.BlackBishop,PieceEnum.BlackHorse,PieceEnum.BlackRook],
    [PieceEnum.BlackPawn,PieceEnum.BlackPawn,PieceEnum.BlackPawn,PieceEnum.BlackPawn,PieceEnum.BlackPawn,PieceEnum.BlackPawn,PieceEnum.BlackPawn,PieceEnum.BlackPawn,],
    [PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty],
    [PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty],
    [PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty],
    [PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty,PieceEnum.Empty],
    [PieceEnum.WhitePawn,PieceEnum.WhitePawn,PieceEnum.WhitePawn,PieceEnum.WhitePawn,PieceEnum.WhitePawn,PieceEnum.WhitePawn,PieceEnum.WhitePawn,PieceEnum.WhitePawn,],
    [PieceEnum.WhiteRook,PieceEnum.WhiteHorse,PieceEnum.WhiteBishop,PieceEnum.WhiteQueen,PieceEnum.WhiteKing,PieceEnum.WhiteBishop,PieceEnum.WhiteHorse,PieceEnum.WhiteRook],
  ],
  selectedPiece: {
    column: -1,
    row: -1,
  },
  selectedCanMove: [{column: -1, row: -1}]
};

@Injectable()
export class BoardStore extends ComponentStore<State> {
  constructor() {
    super(defaultState);
  }

  readonly pieces$ = this.select(({ pieces }) => pieces);
  readonly selectedPiece$ = this.select(({ selectedPiece }) => selectedPiece);
  readonly selectedCanMove$ = this.select(({ selectedCanMove }) => selectedCanMove);


  readonly loadPieces = this.updater((state, pieces: PieceEnum[][]) => ({
    ...state,
    pieces: pieces,
  }));


  public readonly setSelectedPiece = this.updater((state, selectedPiece: ColumnRowEnum) => ({
    ...state,
    selectedPiece: selectedPiece,
  }));

  public readonly updateCanMove = this.updater((state) => ({ 
    ...state,
    selectedCanMove: [],
  }));

  public readonly setCanMove = this.updater((state, selectedCanMove: ColumnRowEnum[]) => ({ 
    ...state,
    selectedCanMove: selectedCanMove
  }));

  public readonly move = this.updater((state, value: PieceEnum[][]) => ({
    ...state,
    pieces: value
  }))
}
