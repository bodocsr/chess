import { Component } from '@angular/core';
import { BoardStore} from './board.store'

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
  providers: [BoardStore]
})
export class BoardComponent {
  constructor(private readonly boardStore: BoardStore){}

  pieces$ = this.boardStore.pieces$;

}
