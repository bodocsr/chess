import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { FieldComponent } from '../field/field.component';
import { BoardComponent } from './board.component';


export default {
  title: 'Components/Board',
  component: BoardComponent,
  decorators: [
    moduleMetadata({
      declarations: [FieldComponent]
    })
  ]
} as Meta;


const Template: Story = (args) => ({
  props: args,
});
export const Primary = Template.bind({});

Primary.args = {
};
