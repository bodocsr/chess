import { Injectable } from '@angular/core';
import { BoardStore } from '../board/board.store';
import { ColumnRowEnum } from 'src/app/enums/column-row-index.enum';
import { PieceEnum } from 'src/app/enums/pieces.enum';

@Injectable()
export class GameLogicService {
    public canMove: ColumnRowEnum[] = []

    public setCanMove(canMove: ColumnRowEnum[]): void{
        this.canMove = this.canMove.concat(canMove)
    }

    public isEmpty(piece: PieceEnum): boolean {
        return piece === PieceEnum.Empty
    }

    public isWhite(piece: PieceEnum): boolean {
        switch(piece){
            case PieceEnum.BlackBishop: return false;
            case PieceEnum.BlackKing: return false;
            case PieceEnum.BlackQueen: return false;
            case PieceEnum.BlackPawn: return false;
            case PieceEnum.BlackHorse: return false;
            case PieceEnum.BlackRook: return false;
            case PieceEnum.WhiteBishop: return true;
            case PieceEnum.WhiteHorse: return true;
            case PieceEnum.WhiteKing: return true;
            case PieceEnum.WhitePawn: return true;
            case PieceEnum.WhiteQueen: return true;
            case PieceEnum.WhiteRook: return true;
        }
        return true;
    }

    public setWhitePawnCanMove(selectedPiece: ColumnRowEnum, pieces: PieceEnum[][]): void{
        this.canMove = []
        if (this.isEmpty(pieces[selectedPiece.column-1][selectedPiece.row])){
            this.setCanMove((selectedPiece.column==6 && (this.isEmpty(pieces[selectedPiece.column-2][selectedPiece.row])))  ? [{column: selectedPiece.column-1, row: selectedPiece.row}, {column: selectedPiece.column-2, row: selectedPiece.row}]: [{column: selectedPiece.column-1, row: selectedPiece.row}]);
        }
        if (!this.isEmpty(pieces[selectedPiece.column-1][selectedPiece.row-1]) && !this.isWhite(pieces[selectedPiece.column-1][selectedPiece.row-1])){
            this.setCanMove(selectedPiece.column==6 ? [{column: selectedPiece.column-1, row: selectedPiece.row-1}, {column: selectedPiece.column-2, row: selectedPiece.row}]: [{column: selectedPiece.column-1, row: selectedPiece.row-1}])
        }
        if (!this.isEmpty(pieces[selectedPiece.column-1][selectedPiece.row+1]) && !this.isWhite(pieces[selectedPiece.column-1][selectedPiece.row+1])){
            this.setCanMove(selectedPiece.column==6 ? [{column: selectedPiece.column-1, row: selectedPiece.row+1}, {column: selectedPiece.column-2, row: selectedPiece.row}]: [{column: selectedPiece.column-1, row: selectedPiece.row+1}])
        }
        this.BoardStore.setCanMove(this.canMove)
        //TODO promotion and en passant
    }

    public setBlackPawnCanMove(selectedPiece: ColumnRowEnum, pieces: PieceEnum[][]): void{
        this.canMove = []
        if (this.isEmpty(pieces[selectedPiece.column+1][selectedPiece.row])){
            this.setCanMove((selectedPiece.column==1 && (this.isEmpty(pieces[selectedPiece.column+2][selectedPiece.row]))) ? [{column: selectedPiece.column+1, row: selectedPiece.row}, {column: selectedPiece.column+2, row: selectedPiece.row}]: [{column: selectedPiece.column+1, row: selectedPiece.row}]);
        }
        if (!this.isEmpty(pieces[selectedPiece.column+1][selectedPiece.row+1]) && this.isWhite(pieces[selectedPiece.column+1][selectedPiece.row-1])){
            this.setCanMove(selectedPiece.column==1 ? [{column: selectedPiece.column+1, row: selectedPiece.row+1}, {column: selectedPiece.column+2, row: selectedPiece.row}]: [{column: selectedPiece.column+1, row: selectedPiece.row+1}])
        }
        if (!this.isEmpty(pieces[selectedPiece.column+1][selectedPiece.row-1]) && this.isWhite(pieces[selectedPiece.column+1][selectedPiece.row+1])){
            this.setCanMove(selectedPiece.column==1 ? [{column: selectedPiece.column+1, row: selectedPiece.row-1}, {column: selectedPiece.column+2, row: selectedPiece.row}]: [{column: selectedPiece.column+1, row: selectedPiece.row-1}])
        }
        this.BoardStore.setCanMove(this.canMove)
        //TODO promotion and en passant
    }

    public setHorseCanMove(selectedPiece: ColumnRowEnum, pieces: PieceEnum[][], isWhite: boolean): void{
        this.canMove = []
        const X: number[] = [ 2, 1, -1, -2, -2, -1, 1, 2 ];
        const Y: number[] = [ 1, 2, 2, 1, -1, -2, -2, -1 ];
        for (let i = 0; i < 8; i++) {
            let x = selectedPiece.column + X[i];
            let y = selectedPiece.row + Y[i];

            if (x >= 0 && y >= 0 && x < 8 && y < 8 && (this.isEmpty(pieces[x][y]) || isWhite !== this.isWhite(pieces[x][y]))){
                this.setCanMove([{column: x, row: y}])
            }
        }
        this.BoardStore.setCanMove(this.canMove)
    }

    public setRookCanMove(selectedPiece: ColumnRowEnum, pieces: PieceEnum[][], isWhite: boolean): void{
        this.canMove = []
        for (let i = selectedPiece.column+1; i < 8; i++) {
                if ( this.isEmpty(pieces[i][selectedPiece.row])){
                    this.setCanMove([{column: i, row: selectedPiece.row}])
                } else {
                    if (this.isWhite(pieces[i][selectedPiece.row]) !== isWhite){
                        this.setCanMove([{column: i, row: selectedPiece.row}])
                    } break;
                    
                } 
        }
        for (let i = selectedPiece.column-1; i >= 0; i--) {
                if ( this.isEmpty(pieces[i][selectedPiece.row])){
                    this.setCanMove([{column: i, row: selectedPiece.row}])
                } else {
                    if (this.isWhite(pieces[i][selectedPiece.row]) !== isWhite){
                        this.setCanMove([{column: i, row: selectedPiece.row}])
                    } break;
                }
        }
        for (let i = selectedPiece.row+1; i < 8; i++) {
                if ( this.isEmpty(pieces[selectedPiece.column][i])){
                    this.setCanMove([{column: selectedPiece.column, row: i}])
                } else {
                    if (this.isWhite(pieces[selectedPiece.column][i]) !== isWhite){
                        this.setCanMove([{column: selectedPiece.column, row: i}])
                    } break;
                }
        }
        for (let i = selectedPiece.row-1; i >= 0; i--) {
                if ( this.isEmpty(pieces[selectedPiece.column][i])){
                    this.setCanMove([{column: selectedPiece.column, row: i}])
                } else {
                    if (this.isWhite(pieces[selectedPiece.column][i]) !== isWhite){
                        this.setCanMove([{column: selectedPiece.column, row: i}])
                    } break;
                }
        }
        this.BoardStore.setCanMove(this.canMove)
    }

    constructor(private readonly BoardStore: BoardStore){}
}
