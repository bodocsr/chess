import { Component, Input } from '@angular/core';
import { ColumnRowEnum } from 'src/app/enums/column-row-index.enum';
import {  PieceEnum } from 'src/app/enums/pieces.enum';
import { BoardStore } from '../board/board.store';
import { GameLogicService } from './game-logic.service'

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.scss'],
  providers: [GameLogicService]
})
export class FieldComponent {
  @Input() public isWhite = false;
  @Input() public piece!: PieceEnum;
  @Input() public FieldIndex!: ColumnRowEnum;
  public isSelected = false;
  PieceEnum = PieceEnum;
  public selectedPiece$ = this.BoardStore.selectedPiece$
  public selectedCanMove$ = this.BoardStore.selectedCanMove$
  public pieces$ = this.BoardStore.pieces$

  public changeSelection(pieces: PieceEnum[][]): void{
    if(PieceEnum.Empty !== this.piece){
      this.BoardStore.setSelectedPiece(this.FieldIndex)
      this.isSelected = !this.isSelected
        switch(this.piece){
          case PieceEnum.WhitePawn: this.gameLogicService.setWhitePawnCanMove(this.FieldIndex, pieces); break;
          case PieceEnum.BlackPawn: this.gameLogicService.setBlackPawnCanMove(this.FieldIndex, pieces); break;
          case PieceEnum.BlackHorse: this.gameLogicService.setHorseCanMove(this.FieldIndex, pieces, false); break;
          case PieceEnum.WhiteHorse: this.gameLogicService.setHorseCanMove(this.FieldIndex, pieces, true); break;
          case PieceEnum.BlackRook: this.gameLogicService.setRookCanMove(this.FieldIndex, pieces, false); break;
          case PieceEnum.WhiteRook: this.gameLogicService.setRookCanMove(this.FieldIndex, pieces, true); break;
        }   
    }
  }

  public checkCanMove(canMove: ColumnRowEnum[]): boolean {
    return canMove.map((value)=>value.column===this.FieldIndex.column && value.row===this.FieldIndex.row).includes(true)
  }

  public moveFigure(canMove: ColumnRowEnum[], selectedPiece: ColumnRowEnum, pieces: PieceEnum[][]): void{
    if(this.checkCanMove(canMove)){
      pieces[this.FieldIndex.column][this.FieldIndex.row] = pieces[selectedPiece.column][selectedPiece.row]
      pieces[selectedPiece.column][selectedPiece.row] = PieceEnum.Empty
      this.BoardStore.move(pieces)
      this.BoardStore.updateCanMove()
      this.isSelected = !this.isSelected
    }
  }


  constructor(private readonly BoardStore: BoardStore, private readonly gameLogicService: GameLogicService){}
}
