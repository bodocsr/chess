import { Meta, Story } from '@storybook/angular';
import { FieldComponent } from './field.component';


export default {
  title: 'Components/Field',
  component: FieldComponent,
} as Meta;


const Template: Story = (args) => ({
  props: args,
});
export const Primary = Template.bind({});

Primary.args = {
};
